'use strict';
var cors = require('cors')

const unleash = require('unleash-server');

var whitelist = process.env.WHITELIST.split(",");

unleash
  .start({
    enableLegacyRoutes: false,
    preRouterHook: app => {
    
        app.use(
            cors({
                credentials: true,
                origin: whitelist
            }),
        )
    },
  })
  .then(unleash => {
    console.log(
      `Unleash started on http://localhost:${unleash.app.get('port')}`,
    );
  });
