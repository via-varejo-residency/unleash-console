FROM registry.access.redhat.com/ubi8/nodejs-10:latest

COPY package.json ./

RUN npm install --production

COPY . .

EXPOSE 4242

CMD node index.js
